package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	_ "github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

type users struct {
	id         int
	username   string
	password   string
	email      string
	created_at time.Time
	status     int
}

var db *sql.DB
var err error

func main() {
	connStr := "host=185.87.49.110 user=admin password=admin123 dbname=test_db sslmode=disable"
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	} else {
		fmt.Println("Successfully connected to database!")
	}
	defer db.Close()

	mux := http.NewServeMux()
	mux.HandleFunc("/", home)
	mux.HandleFunc("/select", selectDatabase)
	mux.HandleFunc("/create", createUser)
	mux.HandleFunc("/update", updateUser)
	mux.HandleFunc("/delete", deleteUser)

	log.Println("Запуск веб-сервера")
	err := http.ListenAndServe(":8080", mux)
	log.Fatal(err)
}

func home(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	w.Write([]byte("Привет!"))
}

func selectDatabase(w http.ResponseWriter, r *http.Request) {
	row := db.QueryRow("SELECT * FROM users")
	us := users{}
	err := row.Scan(&us.id, &us.username, &us.password, &us.email, &us.created_at, &us.status)
	if err != nil {
		panic(err)
	}
	fmt.Println(us.id, us.username, us.password, us.email, us.created_at, us.status)
}

func createUser(w http.ResponseWriter, r *http.Request) {
	create, err := db.Exec("INSERT INTO users (username, password, email, created_at, status) VALUES ('Ivan', 'vanya123', 'vanya@ru', Now(), 2)")
	if err != nil {
		panic(err)
	}
	fmt.Println(create.RowsAffected())
}
func updateUser(w http.ResponseWriter, r *http.Request) {
	update, err := db.Exec("UPDATE users SET username='Vasya' WHERE username='Ivan'")
	if err != nil {
		panic(err)
	}
	fmt.Println(update.RowsAffected())
}

func deleteUser(w http.ResponseWriter, r *http.Request) {
	delete, err := db.Exec("DELETE FROM users")
	if err != nil {
		panic(err)
	}
	fmt.Println(delete.RowsAffected())
}
